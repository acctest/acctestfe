import { BrowserRouter, Routes, Route } from "react-router-dom";
import { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
// import './style/main.  css'

import Register from "./pages/Register";
import NotFoundPage from "./pages/NotFoundPage";
import Index from"./pages/Index";
import AccNav from './components/AccNav'
import Login from "./pages/Login";
import Task from "./pages/Task";
import Cookies from 'universal-cookie';

function App() {
  const [count, setCount] = useState(0)
  const cookies = new Cookies(null, { path: '/' });

  return (
    <>
    <BrowserRouter>
      <AccNav />

      <Routes>
          <Route path="/" Component={Index} />
          <Route path="/task" Component={Task} />
          <Route path="/regis" Component={Register} />
          <Route path="/login" Component={Login} />
          <Route path="/*" Component={NotFoundPage} />
      </Routes>

    </BrowserRouter>
    </>
  )
}

export default App
