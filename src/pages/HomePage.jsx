import { Col, Container, Row } from "react-bootstrap";
import { Routes, Route } from "react-router-dom";
import homeavatar from '../assets/img/homeavatar.png'

import {kelasTerbaru} from '../data/data'

function HomePage(){
    console.log("data kelas terbaru: ", kelasTerbaru);
    return(
        <div className="homepage">
            <header className="w-100 min-vh-100 d-flex align-items-center">
                <Container>
                    <Row className="header-box d-flex align-items-center">
                        <Col lg="6">
                            <h1>with us.
                            <br />
                            <h2><span>doing the best tobe the best</span></h2>
                            <br /></h1>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad tempora quisquam magni enim ...</p>
                            <button className="btn btn-danger btn-lg rounded-1 me-2">lihat promo</button>
                            <button className="btn btn-danger btn-lg rounded-1">Registrasi sekarang</button>
                        </Col>

                        <Col lg="6">
                            <img src={homeavatar} alt="hoe-avatar" />
                        </Col>
                    </Row>
                </Container>
            </header>
            <div className="kelas min-vh-100">
                <Container>
                    <Row>
                        <Col>
                            <h1 className="text-center fw-bold">Kelas Terbaru</h1>
                            <p className="text-center">Menampilkan berbagai kelas terbaru yang akan di selenggarakan</p>
                        </Col>
                    </Row>
                    <Row>
                        {
                            kelasTerbaru.map((kelas) => {
                                <Col key={kelas.id}>
                                    <p>ada</p>
                                </Col>
                            })
                        }
                    </Row>
                </Container>
                
            </div>
        </div>
    );
}
export default HomePage;