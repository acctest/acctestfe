import { Routes, Route } from "react-router-dom";
import { useState, useEffect } from "react";

function AboutPage(){
    const [jadwalSholat, setjadwalSholat] = useState([])

    const fetchUserData = () => {
        fetch("https://api.banghasan.com/sholat/format/json/jadwal/kota/679/tanggal/2023-08-24")
        .then(response => {
            return response.json()
        })
        .then(data => {
            setjadwalSholat(data);
            console.log(jadwalSholat);
        })
    };

    useEffect(()=>{
        fetchUserData();
    });

    return (
        <>
        <h2>About Page</h2>
        <h2> jadwal sholat</h2>
        <h3> asar {jadwalSholat.jadwal.ashar}</h3>
        <h3> azuhur {jadwalSholat.jadwal.dzuhur}</h3>
        <h3> subuh {jadwalSholat.jadwal.subuh}</h3>
        </>
    );
}

export default AboutPage;