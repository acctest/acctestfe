import { useState, useEffect } from "react";
import { Form, Button, Container, Row, Col, Table } from "react-bootstrap";
import Icon_Delete from "../components/Icon_Delete";
import Cookies from 'universal-cookie';
import { useNavigate  } from "react-router-dom";

function Task(){
    const [task, setTask] = useState([]);
    const navigate = useNavigate();

    const cookies = new Cookies(null, { path: '/' });
    let userId;

    if(cookies.get('UserId')){
        userId = cookies.get('UserId');
    }else{
        userId = 9;
        navigate("/login");
    }
    console.log('cookies task: ', cookies.get('UserId'));

    const fetchTask = () => {
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json'},
            mode: 'no-cors'
        };
        fetch(`https://localhost:6001/api/Task/GetTasks?UserId=${userId}`)
            .then((res) => res.json())
            .then((actualData) => setTask(actualData.data))
            .catch(error => {
                //this.setState({ errorMessage: error.toString() });
                alert('There was an error!', error.message);
            });
    };
    useEffect(()=>{
        fetchTask();
    }, []);

    const [titles, setTitles]= useState('');
    const [descriptions, setDescriptions]= useState('');
    const [dueDates, setDueDates]= useState(null);

    const handleAddTask=(e)=>{
        e.preventDefault();
        let requestData = {
            userId: userId,
            title: titles,
            description: descriptions,
            dueDate: new Date(dueDates)
          };
        console.log(requestData);
        if(titles != '' && descriptions != '' && dueDates !=null && userId){
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(requestData),
            };
            fetch('https://localhost:6001/api/Task/AddTask', requestOptions)
            .then((res) => res.json())
            .then((actualData) => {
                fetchTask();
            })
            .catch(error => {
                //this.setState({ errorMessage: error.toString() });
                console.log('There was an error!', error.message);
            });
        }
        else{
            alert("please entry all field.")
        }
    }

    const handleDeleteTask=(id)=>{
        let requestData = {
            taskId: id
          };
        console.log(requestData);
        if(titles != '' && descriptions != '' && dueDates !=null && userId){
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(requestData),
            };
            fetch('https://localhost:6001/api/Task/DeleteTask', requestOptions)
            .then((res) => res.json())
            .then((actualData) => {
                fetchTask();
            })
            .catch(error => {
                //this.setState({ errorMessage: error.toString() });
                console.log('There was an error!', error.message);
            });
        }
        else{
            alert("please entry all field.")
        }
    }
    const handleCompleteTask=(id)=>{
        let requestData = {
            taskId: id
          };
        console.log(requestData);
        if(titles != '' && descriptions != '' && dueDates !=null && userId){
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(requestData),
            };
            fetch('https://localhost:6001/api/Task/CompleteTask', requestOptions)
            .then((res) => res.json())
            .then((actualData) => {
                fetchTask();
            })
            .catch(error => {
                //this.setState({ errorMessage: error.toString() });
                console.log('There was an error!', error.message);
            });
        }
        else{
            alert("please entry all field.")
        }
    }

    const handleUnCompleteTask=(id)=>{
        let requestData = {
            taskId: id
          };
        console.log(requestData);
        if(titles != '' && descriptions != '' && dueDates !=null && userId){
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(requestData),
            };
            fetch('https://localhost:6001/api/Task/UnCompleteTask', requestOptions)
            .then((res) => res.json())
            .then((actualData) => {
                fetchTask();
            })
            .catch(error => {
                //this.setState({ errorMessage: error.toString() });
                console.log('There was an error!', error.message);
            });
        }
        else{
            alert("please entry all field.")
        }
    }
    const handleCheckBox = (id)=>{
        let ischeck = task.filter((t)=> t.taskId === id).isCompleted;
        if(ischeck)
            handleUnCompleteTask(id);
        else    
            handleCompleteTask(id);
    }
    return (
        <>
            <Container>
                <h2>Task page</h2>
                <Row>
                    <Col>
                        <Form>
                        <Form.Group className="mb-3" controlId="titleTask">
                            <Form.Label>Title</Form.Label>
                            <Form.Control type="text" placeholder="Title Task" onChange={e => setTitles(e.target.value)}/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="descTask">
                            <Form.Label>Description</Form.Label>
                            <Form.Control as="textarea" rows={3} onChange={e => setDescriptions(e.target.value)}/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="descTask">
                            <Form.Label>Due Date</Form.Label>
                            <Form.Control type="date"onChange={e => setDueDates(e.target.value)} />
                        </Form.Group>
                        <Button onClick={handleAddTask}>add task</Button>
                        </Form>
                    </Col>
                    <Col>
                    <Table striped bordered hover size="sm">
                    <thead>
                        <tr>
                        <th>Is Done</th>
                        <th>Due Date </th>
                        <th>Title</th>
                        <th>Desc</th>
                        <th>action</th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        task?.map((t) => {  
                            return(
                            <tr key={t.taskId}>
                                <td class='text-center'><Form.Check className={t.isCompleted?'checked': ''} type='checkbox' id={`iscompleted_${t.taskId}`} checked={t.isCompleted} onChange={handleCheckBox(t.taskId)}/></td>
                                <td>{t.dueDate.toString()}</td>
                                <td>{t.title}</td>
                                <td>{t.description}</td>
                                <td className='text-center'><a href={handleDeleteTask(t.taskId)}><Icon_Delete /></a></td>
                            </tr>
                            );
                        })
                    }
                    </tbody>
                    </Table>
                    </Col>
                </Row>
                
            </Container>
            
        </>
    );
}
export default Task;