import { useState } from 'react';
import { Button, Container } from 'react-bootstrap';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Cookies from 'universal-cookie';
import {  useNavigate  } from "react-router-dom";

function Register() {
    const navigate = useNavigate();
    const [userName, setUserName] = useState('');
    const [password, SetPassword] = useState('');
    const cookies = new Cookies(null, { path: '/' });
    console.log('cookies: ', cookies.get('UserId'));

    const handleSubmit = (e) =>{
        e.preventDefault();
        if(userName!= '' && password != ''){
        let requestData = {
            username: userName,
            passwordHash: password,
          };
          console.log(requestData);

          const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(requestData),
            };
            fetch('https://localhost:6001/api/User/Register', requestOptions)
            .then((res) => res.json())
            .then((actualData) => {
                cookies.set('UserId', actualData.data.userId);
                cookies.set('UserName', actualData.data.userName);

                console.log('data', actualData.data);
                console.log('cookies: ', cookies.get('UserId'));
                navigate("/task");
            })
            .catch(error => {
                //this.setState({ errorMessage: error.toString() });
                console.log('There was an error!', error.message);
            });
                
          
        }
        
    }
    return(
        <>
          <Container>
            <Card>
                    <Card.Header>Register Form</Card.Header>
                    <Card.Body>
                        <Form>
                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                <Form.Label>User Name</Form.Label>
                                <Form.Control type="text" placeholder="name@example.com"  onChange={e => setUserName(e.target.value)} />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput2">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" placeholder="Password" onChange={e => SetPassword(e.target.value)} />
                            </Form.Group>
                            <Button type="submit" onClick={handleSubmit}>Register</Button>
                        </Form>
                    </Card.Body>
                </Card>
          </Container>
        </>
    ); 
}

export default Register;