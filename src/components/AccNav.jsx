
import { Navbar, Nav, Container, Row, Col } from "react-bootstrap";
import { NavLink, redirect, useNavigate  } from "react-router-dom";
import Cookies from 'universal-cookie';

function NavbarComp(){
    const navigate = useNavigate();
    const cookies = new Cookies(null, { path: '/' });
    const ClearCookies= ()=> {
        cookies.remove('UserId', { path: '/' });
        cookies.remove('UserName', { path: '/' });
        navigate("/login");
    }

    let linkTask;
    let accountNav = <><NavLink className='me-2' to="/login"> 
                    Login
                </NavLink>
                <NavLink className='me-2' to="/Regis"> 
                    Registration
                </NavLink></>;;
    if(cookies.get('UserId')){
        linkTask = <NavLink className='me-2' to="/task"> 
                Task
            </NavLink>;
        accountNav= <>
        <Row>
            <Col><h3 className="text-light">Hi, {cookies.get('UserName')}</h3></Col>
            <Col><button onClick={ClearCookies}>Logout</button></Col>
        </Row>
        </>;
    }



    return(
        <>
        <Navbar className="bg-dark" expand="lg">
            <Container>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mx-auto text-center">
                    <NavLink className='me-2' to="/"> 
                        Home
                    </NavLink>
                    {linkTask}
                </Nav>
                <div className="text-center">
                    {accountNav}
                </div>
                </Navbar.Collapse>
            </Container>
        </Navbar>
        </>
    );
}
export default NavbarComp;