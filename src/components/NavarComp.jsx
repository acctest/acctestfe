
import { Navbar, Nav, Container } from "react-bootstrap";
import { useState, useEffect } from "react";

import { navLinks } from "../data/data";
import { NavLink } from "react-router-dom";

function NavbarComp(){
    var [bgNavColor, setbgNavColor]= useState(false);
    const onScrollNav= ()=>{
        if(window.scrollY > 10)
            setbgNavColor(true);
        else
            setbgNavColor(false);

    };
    useEffect(()=>{
        onScrollNav();
        window.addEventListener("scroll", onScrollNav);
    })
    return(
        <>
        <Navbar expand="lg" className={bgNavColor? "bg-nav-active": ""}>
            <Container>
                <Navbar.Brand className="fs-3 fw-bold" href="/">eSchool.</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mx-auto text-center">
                    {
                        navLinks.map((link)=>{
                            return (
                            <div className="nav-link" key={link.id}>
                                <NavLink to={link.path} className={({ isActive, isPending }) =>
                                        isPending ? "pending" : isActive ? "active" : ""
                                    } end> 
                                    {link.text}
                                </NavLink>
                            </div>
                            );
                        })
                    }
                    
                </Nav>
                <div className="text-center">
                    <button className="btn btn-outline-danger rounded-1">join us</button>
                </div>
                </Navbar.Collapse>
            </Container>
        </Navbar>
        </>
    );
}
export default NavbarComp;