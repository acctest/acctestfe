import { BrowserRouter, Routes, Route } from "react-router-dom";
import { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import './style/main.css'

import NavbarComp from './components/NavarComp'
import HomePage from './pages/HomePage';
import AboutPage from './pages/AboutPage';
import NotFoundPage from "./pages/NotFoundPage";
 
function App() {
  const [count, setCount] = useState(0)

  return (
    <>
    <BrowserRouter>
      <NavbarComp />

      <Routes>
          <Route path="/" Component={HomePage} />
          <Route path="/about" Component={AboutPage} />
          <Route path="/*" Component={NotFoundPage} />
      </Routes>

    </BrowserRouter>
      
    </>
  )
}

export default App
